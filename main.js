function fetchIssues(){
    
    var issues = JSON.parse(localStorage.getItem('issues'));
    var issuesList = document.getElementById("issuesList");
    
    issuesList.innerHTML = '';
    
    for (var i =0 ; i < issues.length; i++)
        {
            console.log( "Length of issues : " + issues.length);
            var id = issues[i].id;
            var desc = issues[i].description;
            var severity = issues[i].severity;
            var assignedTo = issues[i].assignedTo;
            var status = issues[i].status;
            
            issuesList.innerHTML += '<div class="well"'+
                                    '<h6>Issue ID: ' + id + '</h6>'+
                                    '<p><span class = "label label-info"' + status + '</span></p>'+
                                    '<h3>' + desc + '</h3>'+
                                    '<p><span class="glyphicons glyphicon-time"></span> '+ severity + ' <span class = "glyphicon glyphicon-user"></span> '+ assignedTo + '</p>'+
                                    '<a href="#" class ="btn btn-warning" onclick ="setStatusClosed(\''+id+'\')"> Close </a>' + '<a href="#" class="btn btn-danger" onclick="deleteIssue(\''+id+'\')"> Delete </a>'+'</div>';
        }
}

document.getElementById("issueInputForm").addEventListener('submit',saveIssue);

function saveIssue (e) {
    var issueId = chance.guid()
    var issueDesc = document.getElementById("issueDescInput").value;
    var issueSeverity = document.getElementById("issueSeverityInput").value;
    var issueAssignedTo = document.getElementById("issueAssignedToInput").value;
    var issueStatus = "Open";
    
    var issue = {
        id: issueId,
        description: issueDesc,
        severity: issueSeverity,
        assignedTo: issueAssignedTo,
        status: issueStatus
    }
    
    console.log("issue");
    console.log(issue);
    
    if (localStorage.getItem("issues") === null)
        {
            console.log("issue not found")
            var issues = [];
            issues.push(issue);
            localStorage.setItem("issues", JSON.stringify(issues));
        } else {
            console.log("issues exist")
            var issues = JSON.parse(localStorage.getItem("issues"));
            console.log(issues);
            issues.push(issue);
            localStorage.setItem("issues", JSON.stringify(issues));    
        }
    
    fetchIssues();
    
    e.preventDefault();
}


function saveIssueOrg(e) {
  var issueId = chance.guid();
  var issueDesc = document.getElementById('issueDescInput').value;
  var issueSeverity = document.getElementById('issueSeverityInput').value;
  var issueAssignedTo = document.getElementById('issueAssignedToInput').value;
  var issueStatus = 'Open';
  var issue = {
    id: issueId,
    description: issueDesc,
    severity: issueSeverity,
    assignedTo: issueAssignedTo,
    status: issueStatus
  }
  
  if (localStorage.getItem('issues') === null) {
    var issues = [];
    issues.push(issue);
    localStorage.setItem('issues', JSON.stringify(issues));
  } else {
    var issues = JSON.parse(localStorage.getItem('issues'));
    issues.push(issue);
    localStorage.setItem('issues', JSON.stringify(issues));
  }
  
  document.getElementById('issueInputForm').reset();
 
  fetchIssues();
  
  e.preventDefault(); 
}